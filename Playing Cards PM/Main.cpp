// Ryan Appel

#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

enum Rank
{
	TWO = 2, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE
};

enum Suit { SPADE, HEART, CLUB, DIAMOND };

struct Card
{
	Rank rank;
	Suit suit;
};

void PrintCard(Card card)
{
	cout << "The ";

	switch (card.rank)
	{
	case TWO: cout << "two "; break;
	case THREE: cout << "three "; break;
	case FOUR: cout << "four "; break;
	case FIVE: cout << "five "; break;
	case SIX: cout << "six"; break;
	case SEVEN: cout << "seven "; break;
	case EIGHT: cout << "eight "; break;
	case NINE: cout << "nine "; break;
	case TEN: cout << "ten "; break;
	case JACK: cout << "eleven "; break;
	case QUEEN: cout << "twelve "; break;
	case KING: cout << "thirteen"; break;
	case ACE: cout << "fourteen"; break;
	}

	cout << "of ";

	switch (card.suit)
	{
	case SPADE: cout << "spade "; break;
	case CLUB: cout << "club "; break;
	case HEART: cout << "heart "; break;
	case DIAMOND: cout << "diamond "; break;
	}
}
Card HighCard(Card c, Card c2)
{
	if (c.rank > c2.rank) return c;
	else return c2;
 }
int main()
{
	Card c;
	c.rank = NINE;
	c.suit = CLUB;

	Card c2;
	c2.rank = TEN;
	c2.suit = SPADE;

	// for testing...
	//PrintCard(HighCard(c, c2));

	_getch();
	return 0;
}

